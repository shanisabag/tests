import { expect } from "chai";
import { sum , multiply } from "../src/calc";

describe("The calc module", () => {
    context("sum", () => {
        it("should exist", () => {
            expect(sum).to.be.instanceOf(Function);
        });

        it("should sum two numbers", () => {
            const actual1 = sum(2,3);
            expect(actual1).to.equal(5);
        });

        it("should sum several numbers", () => {
            const actual2 = sum(1,2,3,4,5,6);
            expect(actual2).to.equal(21);
        });
    });

    context("multiply", () => {
        it("should exist", () => {
            expect(multiply).to.be.instanceOf(Function);
        });

        it("should multiply two numbers", () => {
            const actual3 = multiply(2,3,5);
            expect(actual3).to.eql([6,10]);
        });

        it("should multiply several numbers", () => {
            const actual4 = multiply(1,2,3,4,5,6);
            expect(actual4).to.eql([2,3,4,5,6]);
        });
    });
});