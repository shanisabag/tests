export function sum (...numbers : number[] ) : number {
    return numbers.reduce((x, y) => x + y, 0);
} 

export function multiply (multiplier : number, ...numbers : number[]) : number[] {
    return numbers.map(x => x * multiplier);
}